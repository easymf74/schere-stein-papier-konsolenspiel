//schnick_schnack_schnuck-v2.cpp
/* Schere Stein Papier
		1				2			3
  n verliert gegen n+1
  1 verliert gegen 2 
  2 verliert gegen 3
  3 verliert gegen 1
*
*/
  
#include <iostream>  
#include <string>
#include "zufall.h"
#include "input.h"
#include "systembefehle.h"

void printKopf();

int main(int argc, char* argv[]){
	//Konstanten
	enum Spieler
		{
			UNENTSCHIEDEN, 
			MENSCH, 
			COMPUTER
		};
	const short SPIELENDE=9;
	const std::string FIGUREN[] 
		= { 	
				std::string("Schere"),
				std::string("Stein"),
				std::string("Papier") 
			};						
	const std::string PUNKT_FUER[]
		= {		
				std::string("Niemanden!"),
				std::string("Dich!"),
				std::string("den Computer!")
			};
			
	//Variablen
	unsigned short spielerwahl;
	unsigned short computerwahl;
	short sieger;
	Zufall zufall(1,3);
	Menu auswahl
		(
			"* Was wählst Du? [ 1-3 | (9 = SPIELENDE) ] : ",
			"1239"
		);
	unsigned int punkte[3]{};
	
	// Programmstart:
	printKopf();
	do 
	{
		spielerwahl= auswahl.get_short(); 
		if( spielerwahl != SPIELENDE )
		{
			computerwahl=zufall();
			printKopf();
			
			//Auswertung
			sieger
				=	spielerwahl == computerwahl 
						? Spieler::UNENTSCHIEDEN
						: computerwahl == (spielerwahl%3)+1
			/* ist computerwahl Nachfolger von spielerwahl ? 
			 * dann gewinnt der Computer
			 * sonst gewinnt der Mensch
			 */
							? Spieler::COMPUTER
							: Spieler::MENSCH;
			
			++punkte[sieger];
			
			std::cout 
				<< "* Spielstand:  "
				<< punkte[Spieler::MENSCH] 
				<< " für " << PUNKT_FUER[Spieler::MENSCH]
				<< 	"  :  "
				<< punkte[Spieler::COMPUTER] 
				<< " für " << PUNKT_FUER[Spieler::COMPUTER]
				<< "\n****************************************************\n"
			
				<< "* Wahl: "
				<< "Du: " << FIGUREN[spielerwahl-1]
				<< "  /  Computer: " << FIGUREN[computerwahl-1]
				<< "\n*  ---> Punkt für "
				<< PUNKT_FUER[sieger]
			
				<< "  -  Es steht "
				<< punkte[Spieler::MENSCH] 
				<< 	" : "
				<< punkte[Spieler::COMPUTER] 
				<< std::endl;
				
		} // endif (spielerwahl !=SPIELENDE)
	}while( spielerwahl != SPIELENDE );
	printKopf();
	std::cout << "*   Endstand: "
						<< punkte[Spieler::MENSCH] 
						<< " für "
						<< PUNKT_FUER[Spieler::MENSCH]
						<< 	" : "
						<< punkte[Spieler::COMPUTER] 
						<< " für " 
						<< PUNKT_FUER[Spieler::COMPUTER]
						<< std::endl
						<< "***************************************************"
						<< "*\n* Das hat Spaß gemacht :-)\n" << std::endl;
	
	return 0;
}
 
void printKopf(){
ClearKonsole
std::cout  
<< "****************************************************\n"  
<< "*          SCHERE    |  STEIN    |    PAPIER       *\n"
<< "* wähle:     -1-     |   -2-     |     -3-         *\n"
<< "****************************************************\n";	
}

