//zufalltest_main.cpp
//g++ -std=c++14 -Wall zufalltest_main.cpp zufall.cpp -o test
#include <iostream>
#include "zufall.h"

int main()
{
	Zufall zufall(1,6);
	std::cout << "Zufallszahl: " << zufall() << std::endl;
	int auftreten[6]{};
	int zufallsZahl=0;
	
	for(int i=1; i<10000+1; i++)
	{
		auftreten[( zufallsZahl=zufall() )-1]++;
		
		if(i>1000 && i<1031)
		{
			std::cout << zufallsZahl << " ";
		}
	}
	
	std::cout << "\nVerteilung: " << std::endl;
	for(int i=0; i<6; ++i)
	{
		std::cout << i+1  << " : " << auftreten[i] << " mal," << " ";
		if( (i+1)%3==0 ){
			std::cout << '\n';
		}
	}
}
