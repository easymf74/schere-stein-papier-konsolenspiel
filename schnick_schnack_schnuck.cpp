//schnick_schnack_schnuck.cpp
/* Schere Stein Papier
		1				2			3
  n verliert gegen n+1
  1 verliert gegen 2 
  2 verliert gegen 3
  3 verliert gegen 1
*
*/
  
#include <iostream>  
#include <string>
#include "zufall.h"
#include "input.h"
#include "systembefehle.h"

void printKopf();
short getSieger(unsigned short,unsigned short);


int main(int argc, char* argv[]){

	//Variablen
	unsigned short spielerwahl;
	unsigned short computerwahl;
	short sieger;
	std::string gewinner;
	Zufall zufall(1,3);
	Menu auswahl("* Was wählst Du? [ 1-3 | (9 = SPIELENDE) ] : ","1239");
	
	//Konstanten
	const std::string figuren[] = { 	std::string("Schere"),
																		std::string("Stein"),
																		std::string("Papier") };						
	const std::string spieler[]= {		std::string("Dich!"),
																		std::string("den Computer!") };
	unsigned int punkte[2]{};
	printKopf();
	
	do 
	{
		spielerwahl= auswahl.get_short(); 
		if( spielerwahl != 9 )
		{
			computerwahl=zufall();
			printKopf();
			
			//Auswertung
			sieger=getSieger(spielerwahl,computerwahl);
			if (sieger+1) 
			// 0 bei unentschieden, da sieger=-1 bei unentschieden
			{
				++punkte[sieger];
				gewinner = spieler[sieger];
			}else
			{
				gewinner = "niemanden!";
			}
			
			std::cout << "* Spielstand:  "
						  << punkte[0] << " für " << spieler[0]
							<< 	"  :  "
							<< punkte[1] << " für " << spieler[1]
							<< "\n****************************************************"
							<< std::endl;
							
			
			std::cout << "* Wahl: "
								<< "Du: " << figuren[spielerwahl-1]
								<< "  /  Computer: " << figuren[computerwahl-1]
								<< "\n*  ---> Punkt für "
								<< gewinner;
			
			std::cout << "  -  Es steht "
								<< punkte[0] 
								<< 	" : "
								<< punkte[1] 
								<< std::endl;
		} // endif (spielerwahl !=9)
	}while( spielerwahl != 9 );
	printKopf();
	std::cout << "*   Endstand: "
						<< punkte[0] << " für " << spieler[0]
						<< 	" : "
						<< punkte[1] << " für " << spieler[1]
						<< std::endl
						<< "***************************************************"
						<< "*\n* Das hat Spaß gemacht :-)\n" << std::endl;
	
	return 0;
}
  
unsigned short Nachfolger( unsigned short n){	
 // Bestimmt den Nachfolger im Zahlenbereich 1-3
	return (n%3)+1;
}
short getSieger(unsigned short a,unsigned short b){
	//gibt -1 bei unentschieden; 0 für a und 1 für b zurück
		short sieger = -1;
		if (a !=b )
		{
			sieger = Nachfolger(a)==b ? 1 : 0;
		}
		return sieger;
}
 
void printKopf(){
ClearKonsole
std::cout  
<< "****************************************************\n"  
<< "*          SCHERE    |  STEIN    |    PAPIER       *\n"
<< "* wähle:     -1-     |   -2-     |     -3-         *\n"
<< "****************************************************\n";	
}

