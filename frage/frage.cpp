//frage.cpp
#include "frage.h"

Frage::Frage() 
	:txtFrage("Eingabe: "),
	erlaubteEingaben("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,;.:-_#'+*~´`ß?\\=})]([/{&%$§\"!^′°<>@€äöüÄÖÜ")
	{}

Frage::Frage
	(std::string f, std::string eE)
  : txtFrage(f), erlaubteEingaben(eE)
{}

void Frage::setFrage(std::string f, std::string eE){
		txtFrage=f;
		erlaubteEingaben=eE;
}

char Frage::frag() const{
	char wahl=0;
	std::cin.exceptions(std::cin.failbit | std::cin.badbit);
	while (!wahl)
	{
		try
		{
			std::cout << txtFrage;
			std::cin >> wahl;
			
			//Behandlung falsche Eingabe
			if(	erlaubteEingaben.find(wahl) 
								== std::string::npos)
			{
				wahl=0;
			}
		}
		catch (std::istream::failure e)
		{
			wahl=0;
			std::cin.clear();
		}
		std::cin.ignore
				(std::numeric_limits<std::streamsize>::max()
				,'\n');
	}
	return wahl;
}

short Frage::get_short() const{
		return ( frag() - CHAR_TO_SHORT );
}
