//frage-test.cpp
#include "frage.h"
#include <iostream>

int main(int argc, char* argv[]){
	Frage menu(
		"Möchtest du einen (A)pfel oder eine (B)irne? ",
		"aAbB"
	);
	
	char eingabe = menu.frag();
	
	switch (eingabe)
	{
		case 'a':
		case 'A':
			std::cout
			<< "Hier hast du einen Apfel!" << std::endl;
			break;
		case 'b':
		case 'B':
			std::cout
			<< "Birnen gibt es im Laden um die Ecke. "
			<< std::endl;
			break;
		default:
			std::cout << "Das ist unmöglich" << std::endl;
	}
	
	menu.setFrage(
		"Menu:\n 1) Option1\n 2) Option2\n 3) Option3\n",
		"123"
	);
	
	short auswahl = menu.get_short();
	
	if (auswahl>1)
	{
		std::cout << "Wusste ich, dass du nicht die 1 wählst"
		<< std::endl;
	}
	else
	{
		std::cout << "ok, dann also die Option 1."
		<< std::endl;
	}
		
	return 0;
}
