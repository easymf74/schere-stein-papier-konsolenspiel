//zufall-main.cpp
//g++ -std=c++14 -Wall zufall-main.cpp zufall.cpp -o wuerfel
#include <iostream>
#include "zufall.h"

int main(int argc, char* argv[]){
	Zufall zufallszahl(1,100);
	
	std::cout << "Die Zufallszahl zwischen 1 und 100 lautet: "
						<< zufallszahl() << std::endl;
	
	return 0;
}
