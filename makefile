#makefile

C=g++
CF=-c -std=c++14 -Wall

OBJ=main.o\
		zufall.o\
		input.o
		
#~ MAIN=schnick_schnack_schnuck.cpp
MAIN=schnick_schnack_schnuck-v2.cpp
HD=zufall.h	input.h systembefehle.h

all: sss


sss: $(OBJ)
	$(C) $^ -o $@

main.o: $(HD) $(MAIN) zufall.cpp input.cpp
	$(C) $(CF) $(MAIN) -o $@

zufall.o: zufall.cpp zufall.h
	$(C) $(CF) zufall.cpp -o $@

input.o:	input.cpp input.h
	$(C) $(CF)	input.cpp -o $@

clean:
	$(RM) $(OBJ) sss
	
new::
	$(RM) main.o sss
new::	sss
	
	
