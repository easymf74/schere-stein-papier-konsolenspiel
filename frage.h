//frage.h

#ifndef FRAGE_H
#define FRAGE_H
#include <iostream>
#include <string>
#include <limits>

/*
 * Initialisierung:
 *    frage(
 * 						std::string Frage,
 *          	std::string erlaubteEingaben,
 * 				 );
 *	oder
 * 		frage();
 * 		setFrage(
 * 						std::string Frage, 
 * 						std::string erlaubteEingaben
 * 						);
 * Frage:
 *    frag(); --> gibt eine erlaubtes Zeichen zurück
 * 		get_short() -> gibt eine erlaubte Ziffer zurück 
 */

class Frage
{
  std::string txtFrage;
  std::string erlaubteEingaben;
	const short CHAR_TO_SHORT=48;
public:
		Frage();
    Frage(std::string f, std::string eE);
    void setFrage(std::string f, std::string eE);
    char frag() const;
    short get_short() const;
};

#endif // FRAGE_H
