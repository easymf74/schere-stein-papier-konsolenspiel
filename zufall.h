//zufall.h
#include <random>
#include <memory>

/*
 * 	Nutzung der Klasse:
 * 
 *  Initialisierung über:	
 * 		Zufall zufall(min,max);
 * 
 * 	Zufallszahlen abrufen über:
 * 		int zufallszahl; 
 * 		zufallszahl = zufall();
 */

#ifndef zufall_h
#define zufall_h zufall_h

class Zufall
{
	// Zufallsgenerator definieren
	std::default_random_engine generator;
	//std::uniform_int_distribution<int>* verteiler; 
	std::unique_ptr< std::uniform_int_distribution<int> > verteiler; 
public:
	Zufall(int von, int bis);
	//~Zufall(){ delete verteiler;}//unnötig wegen unique_ptr
	
	// obj() gibt die Zufallszahl zurück
	int operator()(); 
};

#endif // ifndef zufall_h
