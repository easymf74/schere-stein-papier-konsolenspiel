//zufall.cpp
#include "zufall.h"

Zufall::Zufall(int von, int bis)
	: verteiler{new std::uniform_int_distribution<int>(von,bis)}
{
	{ // Erläuterungen:
	/* Alternativ zur Initialisierungsliste: Verteiler einrichten über reset
			Die einzige Möglichkeit den Verteiler zur Laufzeit
			über den Konstruktor einzurichten ist per new,
			da uniform_int_distribution nur über seinen Konstruktor
			initialisierbar ist.
		
		1. Möglichkeit über die reset-Methode:
		reset zerstört den aktuellen ptr, wenn vorhanden
		und übernimmt den neuen:
		in diesen Fall gibt es noch keinen alten:
	*/
	//~ verteiler.reset( new std::uniform_int_distribution<int>(von,bis) );
	
	/* 2. Alternative über Zuweisung
			nur die Zuweisung eines unique_ptr möglich (move)
			verteiler 
				= new std::uniform_int_distribution<int>(von,bis); //geht nicht
	// richtig ist: */
	//~ verteiler 
		//~ = std::unique_ptr
				//~ < std::uniform_int_distribution<int> >
					//~ ( new std::uniform_int_distribution<int>(von,bis));
	}				
	// Startwert zeitabhängig setzen
	generator.seed( time(nullptr)*time(nullptr) );
}

int Zufall::operator()()
{ 
	return (*verteiler)(generator);
}
